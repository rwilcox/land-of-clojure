# chap1-guess-my-number

Implements the first game from Land Of Lisp, Guess my Number

## Usage

Land of Lisp will be shorted  as LaL, to avoid confusion with LoL (Let Over Lambda)

This project is documented with [Autodoc](http://tomfaulhaber.github.com/autodoc/)

To generate the autodoc use `lein autodoc`

Run this in the REPL, just like LaL wants, by using `lein repl`!!


## Bending It Like... Ryan

To create a lein project (the make tool for Clojure) like I did:

  1. Download lein and put it somewhere. You could `port install leiningen` (or homebrew), 
    or install the standalone script using the [three simple instructions at the home page](https://github.com/technomancy/leiningen).
    SERIOUSLY, YOU DON'T HAVE TO INSTALL CLOJURE MANUALLY! (Feels a little weird, but read my `project.clj`
  2. `lein new PROJECTNAME`
  3. Specify the stuff I did in `project.clj`
  4. `lein deps`    # installs the dependancies (like Clojure)
  5. `lein repl`
  6. Implement `src/PROJECTNAME/core.clj`

# About

## About me

I've been reading Land Of Lisp in bits and pieces for the last year. The idea was to learn Lisp to have a foundation to learn Clojure.

But recently I had a thought: "I should port the examples from Land Of Lisp to Clojure".

This is not a unique idea, but it does force me to get my feet wet with Clojure. And learn/write stuff down, which is the valuable part, even if just for me.

## Useful Resources I found going from Lisp -> Clojure

[PCL -> Clojure, Chapter 6](http://thinkrelevance.com/blog/2008/09/16/pcl-clojure-chapter-6)

[Common Lisp, Clojure and Evolution](http://www.adrianmouat.com/bit-bucket/2011/02/common-lisp-clojure-and-evolution/)

[ClojureDocs: a Clojure documentation / example repository](http://clojuredocs.org/)


## License

Copyright (C) 2013 Ryan Wilcox

Distributed under the Eclipse Public License, the same as Clojure.
