(ns chap1-guess-my-number.core
	(:require [clojure.math.numeric-tower :as math])
)
; math.numeric-tower has been split out of clojure/contrib as part of an
; effort to split that library apart. You can find instructions
; on its Github page, including lines to paste into the project.clj
; and the require line to get it to work here.
;
; https://github.com/clojure/math.numeric-tower/


; yay for doc strings for some language expressions!
; for example, the definition of the def function is (approximately):
; (def NAME doc-string? EXPR)

(def small
	"you may think def is like Lisp's defparameter, used in LaL. But
	using def for mutable global variables is poor form in Clojure
	(see Clojure Programming Chapter 4) also as Clojure variables are immutable,
	this is poor form indeed.

	This seems to touch on something fundamental to Clojure - a difference
	between identity and state: http://clojure.org/state.

	We need to create a ref here, as the behaviors need to be coordinated
	(theoretically guesses happening at the same time should cooperate with
	each other, even though that won't happen), and synchronous (because it's easy).
	"

	(ref 1)
)


(def big
	"We do NOT use earmuffs like LaL does for these variables.
	We could but that would be:

	  a. poor form
	  b. Generate warnings about 'not declared dynamic' under Clojure 1.3+

	We could ignore form and do it like this:

	(def ^:dynamic *big* 100)
	; Thanks: <http://stackoverflow.com/a/8224319/224334>

	But setf doesn't exist in Clojure so you'd need to find another way to do
	the setting (call def again???). Which sounds like more pain than using refs"

	(ref 100)
)

(defn guess-my-number []
	(math/round (/ (+ (deref small) (deref big)) 2)) ; use a simple average instead of ash
)

(defn smaller []
	(dosync
		(ref-set big (- (guess-my-number) 1))  ; 1- doesn't seem to exist in Clojure??!
	) ; ref-sets need to be wrapped in transactions

	(guess-my-number)
)

(defn bigger []
	(dosync
		(ref-set small (+ (guess-my-number) 1))
	)

	(guess-my-number)
)

(defn start-over []
	(dosync (ref-set small 1) (ref-set big 100))

	(guess-my-number)
)
