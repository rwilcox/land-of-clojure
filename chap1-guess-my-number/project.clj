(defproject chap1-guess-my-number "1.0.0-SNAPSHOT"
  :description "Implementation of Guess My Number from Land Of Lisp"
  :dependencies [
  		[org.clojure/clojure "1.4.0"]              ; lein downloads the version of the language specified and shoves it in classpath. No more "make sure you have the right version of Language X installed for this project"!!!
	  	[org.clojure/math.numeric-tower "0.0.2"]   ; need for round
  	]
  :dev-dependencies [[lein-autodoc "0.9.0"]]       ; we could get pretty documentation if we wanted!!
  :main chap1-guess-my-number.core)                ; the main declaration automatically loads our core so things like REPL see it
